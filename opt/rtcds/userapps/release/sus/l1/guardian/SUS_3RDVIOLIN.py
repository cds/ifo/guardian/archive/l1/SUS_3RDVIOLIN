from guardian import GuardState
from guardian import NodeManager
from time import sleep
import subprocess
import time
import os
import cdsutils

# v1 - S. Aston - 04/01/2019
# Script for LLO to manage SUS violin modes

# v2 - S. Aston - 04/03/2019
# Added DAMP ALL and UNDAMP ALL states
# Added some notifications for completing DAMP and UNDAMP all completion and if the overall VM damping switch is off

# v3 - S. Aston - 04/04/2019
# Enabled "goto" states for each specific violin mode order damping or un-damping, but leave damp/undamp all as requestable states only

# v4 - S. Aston - 04/09/2019
# Make almost everything a "goto" state, make "damp_3rd" nominal and remove "damp_all"
# Reduced notify sleeps from 5 to 3 seconds to help expedite things
 
# v5 - S. Aston - 04/11/2019
# Based on SUS_VIOLIN.py generated a dedicated Guardian node for just 3rd order violin mode damping

# v6 - S. Aston - 05/21/2019
# Added violin mode warnings to monitor damping of modes above a defined threshold

# v7 - S. Aston - 06/25/2019
# Enabled violin mode monitoring to automatically shut down damping of modes above a defined threshold to try and avoid a lock-loss

# TODO:
# Consider adding some alarms and alerts after reaching a threshold?
# Can we speed up setting up filters by restoring EDB snapshots of violin modes?

# Define all QUAD suspensions
quads = ['ITMX','ITMY','ETMX','ETMY']

# Define 1st 2nd and 3rd order mode numbers
modes1 = ['3', '4', '5', '6']
modes2 = ['11', '12', '13', '14']
modes3 = ['21', '22', '23', '24']

# Nominal operational state
nominal = 'DAMP_3RD'

# Initial request on initialization
request = 'IDLE'

class INIT(GuardState):
    def main(self):
        return True

class IDLE(GuardState):
    goto = True
    request = True
    
    def main(self):
        return True

class DAMP_3RD(GuardState):
    goto = True
    request = True

    def main(self):
        #if ezca['SUS-ITMX_L2_DAMP_OUT_SW'] == 0:
        #    notify("Violin mode damping switch is disabled")

        for sus in quads:
            for mode in modes3:
		# Zero the gains for all QUADs, just in case damping is already engaged
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
        sleep(3.0)

        for sus in quads:
            for mode in modes3:
                # Clear damping filter histories for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RSET'] = 2
                # Clear monitor filter histories for all QUADs
                #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
                #ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2
                
        # Set ITMX damping filters
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE21', 'FM1', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE22', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE23', 'FM3', 'FM10', 'ON')
 
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMX_L2_DAMP_MODE24', 'FM4', 'FM10', 'ON')

        
        # Set ITMY damping filters
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE21', 'FM1', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE22', 'FM3', 'FM10', 'ON')
 
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE23', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FMALL', 'OFF')
        ezca.switch('SUS-ITMY_L2_DAMP_MODE24', 'FM3', 'FM10', 'ON')

        
        # Set ETMX damping filters
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE21', 'FM1', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE22', 'FM2', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE23', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMX_L2_DAMP_MODE24', 'FM2', 'FM6', 'FM10', 'ON')
  
        
        # Set ETMY damping filters
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE21', 'FM4', 'FM10', 'ON')
        
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE22', 'FM2', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE23', 'FM3', 'FM10', 'ON')

        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FMALL', 'OFF')
        ezca.switch('SUS-ETMY_L2_DAMP_MODE24', 'FM3', 'FM10', 'ON')

        
        # Engage damping filter outputs for all QUADs
        for sus in quads:
            for mode in modes3:
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'ON')

        # Set ITMX damping filter gains
        ezca['SUS-ITMX_L2_DAMP_MODE21_GAIN'] = -1000
        ezca['SUS-ITMX_L2_DAMP_MODE22_GAIN'] = 1000
        ezca['SUS-ITMX_L2_DAMP_MODE23_GAIN'] = -1000
        ezca['SUS-ITMX_L2_DAMP_MODE24_GAIN'] = 1000
        
        # Set ITMY damping filter gains
        ezca['SUS-ITMY_L2_DAMP_MODE21_GAIN'] = -500 # changed on 3/22/2024
        ezca['SUS-ITMY_L2_DAMP_MODE22_GAIN'] = 1000
        ezca['SUS-ITMY_L2_DAMP_MODE23_GAIN'] = 1000
        ezca['SUS-ITMY_L2_DAMP_MODE24_GAIN'] = 1000
        
        # Set ETMX damping filter gains
        ezca['SUS-ETMX_L2_DAMP_MODE21_GAIN'] = 0
        ezca['SUS-ETMX_L2_DAMP_MODE22_GAIN'] = 1000
        ezca['SUS-ETMX_L2_DAMP_MODE23_GAIN'] = 1000
        ezca['SUS-ETMX_L2_DAMP_MODE24_GAIN'] = 2000
        
        # Set ETMY damping filter gains
        ezca['SUS-ETMY_L2_DAMP_MODE21_GAIN'] = -1000
        ezca['SUS-ETMY_L2_DAMP_MODE22_GAIN'] = -1000
        ezca['SUS-ETMY_L2_DAMP_MODE23_GAIN'] = 500 # changed 3/22/2024
        ezca['SUS-ETMY_L2_DAMP_MODE24_GAIN'] = -500 # changed 3/22/2024
        
        notify("3rd order violin mode damping engaged")
        sleep(3.0)

    def run(self):
        for sus in quads:
            for mode in modes3:
                level = ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_RMSLP_LOG10_OUTMON']
                index = ezca['GRD-ISC_LOCK_STATE_N']
                if (level >= -16.5) & (index >= 1100):
                    #notify("Warning! "+sus+" violin mode "+mode+" above threshold, gain set to zero")
                    notify("Warning! "+sus+" violin mode "+mode+" elevated")
                    #ezca['SUS-'+sus+'_L2_DAMP_MODE'+mode+'_GAIN'] = 0
        return True

class UNDAMP_3RD(GuardState):
    goto = True
    request = True
    
    def main(self):
        for sus in quads:
            for mode in modes3:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("3rd order violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

class UNDAMP_ALL(GuardState):
    goto = True
    request = True

    def main(self):
        for sus in quads:
            for mode in modes1 + modes2 + modes3:
                # Zero the gains for all QUADs
                ezca['SUS-' + sus + '_L2_DAMP_MODE' + mode + '_GAIN'] = 0
                # Disengage damping filter outputs for all QUADs
                ezca.switch('SUS-' + sus + '_L2_DAMP_MODE' + mode, 'OUTPUT', 'OFF')
        notify("All violin mode damping disengaged")
        sleep(3.0)

    def run(self):
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','DAMP_3RD'),
    ('IDLE','UNDAMP_3RD'),
    ('IDLE','UNDAMP_ALL'),
    ]

# edges += [('DAMP_3RD','DAMP_3RD')]
edges += [('UNDAMP_3RD','UNDAMP_3RD')]
edges += [('UNDAMP_ALL','UNDAMP_ALL')]
